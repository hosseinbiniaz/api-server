FROM python:3.6-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/

RUN pip3 install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

# Fix Bug in openapi installed with connexion v2.7.0
COPY fix/openapi.py /usr/local/lib/python3.6/site-packages/connexion/operations/openapi.py

EXPOSE 8080

ENTRYPOINT ["python3"]

CMD ["-m", "swagger_server"]