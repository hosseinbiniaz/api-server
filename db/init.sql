CREATE DATABASE hpc_api;

USE hpc_api;

SET NAMES utf8;

SET time_zone = "+00:00";

SET foreign_key_checks = 0;

SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS job;

CREATE TABLE job (
    job_id INT(10) NOT NULL AUTO_INCREMENT,
    hpc_job_id INT(10) DEFAULT NULL,
    operation VARCHAR(50) NOT NULL,
    user_id INT(10) NOT NULL,
    name VARCHAR(250) NOT NULL,
    commands TEXT NOT NULL,
    job_meta_data TEXT,
    created DATETIME NOT NULL,
    updated DATETIME NOT NULL,
    result TEXT,
    log TEXT,
    status VARCHAR(50) NOT NULL,
    PRIMARY KEY (job_id)
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
DROP TABLE IF EXISTS user;
CREATE TABLE user (
    user_id INT(11) NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    PRIMARY KEY (user_id)
)  ENGINE=INNODB DEFAULT CHARSET=LATIN1;
commit;

