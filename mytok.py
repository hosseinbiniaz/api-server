#!/usr/bin/env python3

from swagger_server.models.job import Job  # noqa: E501
import subprocess
import os
import time

#this file should be placed in the folder where the python3 runs server as module (i.e. 1 level higher than main)

# our hardcoded mock "Bearer" access tokens
TOKENS = {
    '123': 'jdoe',
    '456': 'rms'
}

SCOPE_MAPPING = {
    "GET_JobStatus" : '', #could be about getting the job status by cronjob? or the user side?
    "UPDATE_JobStatus" : 'job/update_job_scope, job/update_job_by_operation_scope',
    "GET_Job" : 'job/get_job_by_id_scope, job/find_jobs_by_status_scope',
    "POST_Code" : 'file/upload_file_scope', #cronjob needs both
    "GET_Code" : 'file/get_file_scope', #cronjob needs both
    "POST_Job" : 'job/add_job_scope', 
    "UPDATE_Job" : 'job/update_job_scope, job/update_job_by_operation_scope',
    "DELETE_Job" : 'job/delete_job_scope'
}
'''
job/find_jobs_by_status_scope --> used in cronjob line 65 to find new jobs
job/add_job_scope #by hpc.py
job/update_job_by_operation_scope --> added to update job 
job/get_job_by_id_scope #used by hpc.py to get jobs
job/update_job_scope --> mapped from two 'checkboxes' i.e. UPDATE_job and UPDATE_JobStatus
job/delete_job_scope
user/login_user_scope --> NA 
file/upload_file_scope --> needed by both scripts
file/get_file_scope --> needed by both scripts
'''

# lets say the token is checked for accessing sensitive endpoint and returns true:
# run a js file in shell:
# first get the fcm_data from db - make connection using job object : like job_controller does for fcm_endpoint_receive(body)
def sensitive_func():
    time.sleep(5)
    job = Job()
    cox = job.connect()
    if cox != '':
        print('cox != 0')
        error_code = 500
        error_message = {
            "detail": cox,
            "status": error_code,
            "title": "Internal Server Error",
            "type": "about:blank"
        }
        print(error_message, error_code)

    #then return the actual rows of the last entry of the fcm_data table
    fcm_data_for_pushjs = job.retrieve_pushjs_fcm_data()
    #list with 1 element of type tuple
    # print(type(fcm_data_for_pushjs[0]))
    # print(fcm_data_for_pushjs)
    # #index col:
    # print(fcm_data_for_pushjs[0][0])
    # #endpoint col:
    # print(fcm_data_for_pushjs[0][2])
    # print(type(fcm_data_for_pushjs[0][2]))

    #parse data to be passed 
    endpoint = fcm_data_for_pushjs[0][1]
    expirationTime = fcm_data_for_pushjs[0][2]
    if expirationTime == 'NULL':
        expirationTime = 'null'
    p256dh_key = fcm_data_for_pushjs[0][3]
    auth_key = fcm_data_for_pushjs[0][4]

    #print (endpoint,'\n\n', expirationTime, '\n\n', p256dh_key, '\n\n', auth_key)

    args = endpoint + ' ' + expirationTime + ' ' + p256dh_key + ' ' + auth_key
    print(args)

    #now pass in this info as arguemnt to push.js
    path = os.getcwd()
    cmd1 = 'node {path}/push.js {args}'.format(path = '~/mbiniaz/src', args = args)

    # #run the node push command and wait until it returns 
    p1 = subprocess.Popen(cmd1, shell=True)
    p1.wait()


def token_info(access_token) -> dict:
    # connect db
    job = Job()
    cox = job.connect()
    if cox != '':
        print('cox != 0')
        error_code = 500
        error_message = {
            "detail": cox,
            "status": error_code,
            "title": "Internal Server Error",
            "type": "about:blank"
        }
        print(error_message, error_code)

    # get permissions for input access_token line 1017 of jobcontroller.py
    permissions = job.get_token_permissions(access_token)[0][0]

    # parse and return scope
    # permissions = job.get_token_permissions(t)[0][0]
    # print('permissions are: \n')

    #convert to dictionary (it is string, so eval() and take values for allowed token operations)
    perm = eval(permissions)['token_operations']

    # sequence of permissions for swagger to read and act according to oauth scopes defined 
    
    perm_seq = ''
    for item in perm:
        dict_val = SCOPE_MAPPING[str(item)]
        perm_seq = perm_seq + str(dict_val) + ', '

    print('\n\nfrom mytok.py, permissions are:',perm_seq[:-2],'and perm is', perm,'\n\n')
    
    return {"scope": perm_seq[:-2]}


    # uid = TOKENS.get(access_token)
    # #if not uid:
    # #    print('error here')
    # #    return None
    # if access_token == '123':
    #     return {"scope": "job/find_jobs_by_status_scope, file/upload_file_scope" }
    #     # return {"scope": "job/find_jobs_by_status_scope"}

    #     #return {"uid": uid+'1', "scope": "file/upload_file_scope"}
    # elif access_token == '456': 
    #     #run the sensitive function to push notification to frontend
    #     sensitive_func()
    #     #check last col of db (i.e. newest version of the fcm_endpoint/fcm_data)
    #     job = Job()
    #     cox = job.connect()
    #     if cox != '':
    #         print('cox != 0')
    #         error_code = 500
    #         error_message = {
    #             "detail": cox,
    #             "status": error_code,
    #             "title": "Internal Server Error",
    #             "type": "about:blank"
    #         }
    #         print(error_message, error_code)
    #     #wait 7 more seconds before looking for a response
    #     time.sleep(7)
    #     response = job.retrieve_pushresponse()
    #     # print("one white space {r} one white space".format(r = response[0][0]))
    #     if 'yes' in response[0][0]:
    #         print('response is yes')
    #         return {"scope": "file/upload_file_scope"}
    #     else:
    #         print('response is not yes')
    #         return {"scope": "job/find_jobs_by_status_scope"}
    #     #return {"uid": uid+'1', "scope": "file/upload_file_scope"}
    # elif access_token == '789': 
    #     print('access_token is 123 here and you CANNOT access file//uploadfile endpoint')
    #     return {"scope": "file/upload_file_scope"}
    # elif access_token == '101112': 
    #     print('access_token is 123 here and you CANNOT access file//uploadfile endpoint')
    #     return {"scope": "file/upload_file_scope"}
    # elif access_token == '131415': 
    #     print('access_token is 123 here and you CANNOT access file//uploadfile endpoint')
    #     return {"scope": "file/upload_file_scope"}
    # elif access_token == '161718': 
    #     print('access_token is 123 here and you CANNOT access file//uploadfile endpoint')
    #     return {"scope": "file/upload_file_scope"}
    # elif access_token == '192021': 
    #     print('access_token is 123 here and you CANNOT access file//uploadfile endpoint')
    #     return {"scope": "file/upload_file_scope"}
    # elif access_token == '222324': 
    #     print('access_token is 123 here and you CANNOT access file//uploadfile endpoint')
    #     return {"scope": "file/upload_file_scope"}
    # elif access_token == '252627': 
    #     print('access_token is 123 here and you CANNOT access file//uploadfile endpoint')
    #     return {"scope": "file/upload_file_scope"}