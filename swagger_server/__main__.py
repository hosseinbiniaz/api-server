#!/usr/bin/env python3

import connexion

#import os

from swagger_server import encoder

from flask_cors import CORS #https://github.com/spec-first/connexion/issues/357

#pip install -U flask-cors from https://enable-cors.org/server_flask.html


def main():
    app = connexion.App(__name__, specification_dir='./swagger/')
    #add this for CORS support
    CORS(app.app)
    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'HPC RESTful API using Slurm'}, pythonic_params=True)
    

    #print('\n\n\n', os.getcwd(), '\n\n\n')

    app.run(port=8080, debug=False, ssl_context=('swagger_server/.cert/fullchain.pem', 'swagger_server/.cert/privkey.pem') )
    #app.run(port=8080, debug=False)

if __name__ == '__main__':
    main()
