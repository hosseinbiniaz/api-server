openapi: 3.0.0
info:
  title: HPC RESTful API using Slurm
  description: |
    Design and implementation of an API to ease the use of HPC systems.
  termsOfService: https://www.gwdg.de/about-us/catalog/terms-and-conditions
  contact:
    name: Sven Bingert
    email: sven.bingert@gwdg.de
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  version: 1.0.0
servers:
- url: http://hpc-api.open-forecast.eu:8080/HPC-RESTful/1.0.0
  description: HPC RESTful API using Slurm
tags:
- name: job
  description: Everything about HPC jobs
- name: file
  description: Managing files
- name: user
  description: Operations about users
paths:
  /job/findJobsByStatus:
    get:
      tags:
      - job
      summary: Finds jobs, optionally by status
      description: Multiple status values can be provided with comma separated strings
      operationId: find_jobs_by_status
      parameters:
      - name: status
        in: query
        description: Status values that need to be considered for filter
        required: false
        style: form
        explode: true
        schema:
          type: array
          items:
            type: string
            enum:
            - new
            - cronjob_in_progress
            - hpc_queued
            - hpc_in_progress
            - cronjob_failed
            - hpc_failed
            - hpc_aborted
            - completed
      - name: pageLength
        in: query
        description: Number of records to return
        required: true
        style: form
        explode: true
        schema:
          type: integer
          format: int64
      - name: pageNumber
        in: query
        description: Start index for paging
        required: true
        style: form
        explode: true
        schema:
          type: integer
          format: int64
      - name: accessToken
        in: query
        description: Access token
        required: true
        style: form
        explode: true
        schema:
          type: string
      responses:
        "200":
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/inline_response_200'
            application/xml:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Job'
        "400":
          description: Invalid status value
        "401":
          description: Unauthorized
      x-openapi-router-controller: swagger_server.controllers.job_controller
      security:
        - secret_auth:
          - job/find_jobs_by_status_scope
  /job:
    post:
      tags:
      - job
      summary: Schedules a new job to the HPC system
      operationId: add_job
      parameters:
      - name: accessToken
        in: query
        description: Access token
        required: true
        style: form
        explode: true
        schema:
          type: string
      requestBody:
        $ref: '#/components/requestBodies/Job'
      responses:
        "200":
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Job'
            application/xml:
              schema:
                $ref: '#/components/schemas/Job'
        "401":
          description: Unauthorized
        "405":
          description: Invalid input
      x-openapi-router-controller: swagger_server.controllers.job_controller
      security:
        - secret_auth:
          - job/add_job_scope
  /job/updateByOperation/{jobId}:
    put:
      tags:
      - job
      summary: Updates operation of an existing job
      operationId: update_job_by_operation
      parameters:
      - name: jobId
        in: path
        description: name that need to be updated
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int64
      - name: operation
        in: query
        description: Job Operation
        required: true
        style: form
        explode: true
        schema:
          type: string
          enum:
          - queue
          - abort
      - name: accessToken
        in: query
        description: Access token
        required: true
        style: form
        explode: true
        schema:
          type: string
      responses:
        "200":
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Job'
            application/xml:
              schema:
                $ref: '#/components/schemas/Job'
        "400":
          description: Invalid ID supplied
        "401":
          description: Unauthorized
        "404":
          description: Job not found
        "405":
          description: Validation exception
        "409":
          description: Job cannot be updated because of conflict with the current
            state of the resource
      x-openapi-router-controller: swagger_server.controllers.job_controller
      security:
        - secret_auth:
          - job/update_job_by_operation_scope
  /job/{jobId}:
    get:
      tags:
      - job
      summary: Finds job by ID
      description: Returns a single job
      operationId: get_job_by_id
      parameters:
      - name: jobId
        in: path
        description: ID of job to return
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int64
      - name: accessToken
        in: query
        description: Access token
        required: true
        style: form
        explode: true
        schema:
          type: string
      responses:
        "200":
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Job'
            application/xml:
              schema:
                $ref: '#/components/schemas/Job'
        "400":
          description: Invalid ID supplied
        "401":
          description: Unauthorized
        "404":
          description: Job not found
      x-openapi-router-controller: swagger_server.controllers.job_controller
      security:
        - secret_auth:
          - job/get_job_by_id_scope
    put:
      tags:
      - job
      summary: Updates an existing job
      operationId: update_job
      parameters:
      - name: jobId
        in: path
        description: ID of job to return
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int64
      - name: accessToken
        in: query
        description: Access token
        required: true
        style: form
        explode: true
        schema:
          type: string
      requestBody:
        $ref: '#/components/requestBodies/Job'
      responses:
        "200":
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Job'
            application/xml:
              schema:
                $ref: '#/components/schemas/Job'
        "400":
          description: Invalid ID supplied
        "401":
          description: Unauthorized
        "404":
          description: Job not found
        "405":
          description: Validation exception
        "409":
          description: Job cannot be updated because of conflict with the current
            state of the resource
      x-openapi-router-controller: swagger_server.controllers.job_controller
      security:
        - secret_auth:
          - job/update_job_scope
    delete:
      tags:
      - job
      summary: Deletes an existing job
      operationId: delete_job
      parameters:
      - name: jobId
        in: path
        description: Job id to delete
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int64
      - name: accessToken
        in: query
        description: Access token
        required: true
        style: form
        explode: true
        schema:
          type: string
      responses:
        "200":
          description: Successful operation
        "400":
          description: Invalid ID supplied
        "401":
          description: Unauthorized
        "404":
          description: Job not found
        "409":
          description: Job cannot be deleted because of conflict with the current
            state of the resource
      x-openapi-router-controller: swagger_server.controllers.job_controller
      security:
        - secret_auth:
          - job/delete_job_scope
  /user/login:
    get:
      tags:
      - user
      summary: Logs user into the system
      operationId: login_user
      parameters:
      - name: username
        in: query
        description: The user name for login
        required: true
        style: form
        explode: true
        schema:
          type: string
      - name: password
        in: query
        description: The password for login in clear text
        required: true
        style: form
        explode: true
        schema:
          type: string
      responses:
        "200":
          description: Successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/User'
                x-content-type: application/json
            application/xml:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/User'
        "400":
          description: Invalid username/password supplied
      x-openapi-router-controller: swagger_server.controllers.user_controller
      security:
        - secret_auth:
          - user/login_user_scope
  /file/uploadFile:
    post:
      tags:
      - file
      summary: uploads a file
      operationId: upload_file
      parameters:
      - name: filename
        in: query
        description: Name of the file
        required: true
        style: form
        explode: true
        schema:
          type: string
      - name: accessToken
        in: query
        description: Access token
        required: true
        style: form
        explode: true
        schema:
          type: string
      requestBody:
        content:
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/body'
      responses:
        "200":
          description: Successful operation
        "400":
          description: Invalid name supplied
        "401":
          description: Unauthorized
      x-openapi-router-controller: swagger_server.controllers.file_controller
      security:
        - secret_auth:
          - file/upload_file_scope
  /file/{jobId}/getFile:
    get:
      tags:
      - file
      summary: downloads a file
      operationId: get_file
      parameters:
      - name: jobId
        in: path
        description: name that need to be updated
        required: true
        style: simple
        explode: false
        schema:
          type: integer
          format: int64
      - name: fileType
        in: query
        description: Input or output file
        required: true
        style: form
        explode: true
        schema:
          type: string
      - name: accessToken
        in: query
        description: Access token
        required: true
        style: form
        explode: true
        schema:
          type: string
      responses:
        "200":
          description: Successful operation
          content:
            application/octet-stream:
              schema:
                type: string
                format: binary
                x-content-type: application/octet-stream
        "400":
          description: Invalid ID supplied
        "401":
          description: Unauthorized
        "404":
          description: Job not found
      x-openapi-router-controller: swagger_server.controllers.file_controller
      security:
        - secret_auth:
          - file/get_file_scope
  /fcm_data:
    post:
      summary: receive the fcm endpoint and auth-data for msg encryption regarding pushmanager -> subscription obejct from client DOM
      operationId: fcm_endpoint_receive
      requestBody:
        description: push (-notification) subscription object needed to comit to db for node push-for-py.js to work 
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/fcm_data'
        required: true
      responses:
        "200":
          description: Successful operation
          content:
            application/octet-stream:
              schema:
                type: string
                format: binary
                x-content-type: application/octet-stream
        "400":
          description: Invalid ID supplied
        "401":
          description: Unauthorized
        "404":
          description: Job not found
      x-openapi-router-controller: swagger_server.controllers.job_controller
  /pushresponse:
    post:
      summary: test endpoint for getting back push notification response / confirmation
      operationId: push_response
      requestBody:
        description: response of the push notification
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/pushResponse'
        required: true
      responses:
        "200":
          description: Successful operation
          content:
            application/octet-stream:
              schema:
                type: string
                format: binary
                x-content-type: application/octet-stream
        "400":
          description: Invalid ID supplied
        "401":
          description: Unauthorized
        "404":
          description: Job not found
      x-openapi-router-controller: swagger_server.controllers.job_controller
  /token_data:
    post:
      summary: receive request for generating new token from webUI
      operationId: tokenrequest_receive
      requestBody:
        description: contains desired token_operations and project_name from existing or new project from webUI
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/token_data'
        required: true
      responses:
        "200":
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Token_resp'
            application/xml:
              schema:
                $ref: '#/components/schemas/Token_resp'
        "400":
          description: Invalid ID supplied
        "401":
          description: Unauthorized
        "404":
          description: Job not found
      x-openapi-router-controller: swagger_server.controllers.job_controller
components:
  schemas:
    token_data:
      type: object
      properties:
        project_name:
          type: string
        token_operations:
          type: array
          items:
            type: string
            enum:
            - GET_JobStatus
            - UPDATE_JobStatus
            - GET_Job
            - POST_Code
            - GET_Code
            - POST_Job
            - UPDATE_Job
            - DELETE_Job
    fcm_data:
      type: object
      properties:
        endpoint:
          type: string
        expirationTime:
          type: integer
          nullable: true
        keys:
          type: object
          properties: 
            p256dh:
              type: string
              description: p256dh and auth for sending message using push service
            auth:
              type: string
              description: auth generated by push-service for sending push notification messages from server 
    pushResponse:
      type: object
      properties:
        userResponse:
          type: string
      description: the response of the user from authenticator web UI
    commands:
      type: array
      description: commands type and parameters
      items:
        $ref: '#/components/schemas/SubJob'
    JobMetadata:
      required:
      - error
      - output
      type: object
      properties:
        error:
          type: string
        output:
          type: string
      example:
        output: output
        error: error
    PaginationResponse:
      type: object
      properties:
        totalPages:
          type: number
        currentPage:
          type: number
        pageLength:
          type: number
        jobs:
          type: array
          items:
            $ref: '#/components/schemas/Job'
      xml:
        name: PaginationResponsedata
    User:
      required:
      - email
      - firstName
      - lastName
      - password
      - username
      type: object
      properties:
        userId:
          type: integer
          format: int64
        username:
          type: string
        firstName:
          type: string
        lastName:
          type: string
        email:
          type: string
        password:
          type: string
        token:
          type: string
        userStatus:
          type: string
          description: User Status
          enum:
          - active
          - inactive
          - blocked
      example:
        firstName: firstName
        lastName: lastName
        password: password
        userStatus: active
        userId: 0
        email: email
        username: username
        token: token
      xml:
        name: User
    SubJob:
      required:
      - parameters
      - subJobType
      type: object
      properties:
        subJobType:
          type: string
          description: Sub Job Type
          enum:
          - hpc
          - archive
          - unarchive
          - copy
          - compile
          - shell
        parameters:
          type: string
          description: Job parameters
      example:
        subJobType: hpc
        parameters: parameters
    Token_resp:
      type: object
      properties:
        token_value:
          type: string
    Job:
      required:
      - commands
      - name
      - userId
      type: object
      properties:
        jobId:
          type: integer
          format: int64
        hpcJobId:
          type: integer
          format: int64
        operation:
          type: string
          description: Job Operation
          enum:
          - queue
          - abort
        userId:
          type: integer
          description: User Id or GWDG account number
          format: int64
        name:
          type: string
        commands:
          $ref: '#/components/schemas/commands'
        jobMetaData:
          $ref: '#/components/schemas/JobMetadata'
        created:
          type: string
          format: date-time
        updated:
          type: string
          format: date-time
        result:
          type: string
        log:
          type: string
        status:
          type: string
          description: Job Status
          enum:
          - new
          - cronjob_in_progress
          - hpc_queued
          - hpc_in_progress
          - cronjob_failed
          - hpc_failed
          - hpc_aborted
          - completed
      example:
        result: result
        jobId: 0
        log: log
        created: 2000-01-23T04:56:07.000+00:00
        hpcJobId: 6
        name: name
        jobMetaData:
          output: output
          error: error
        operation: queue
        userId: 1
        updated: 2000-01-23T04:56:07.000+00:00
        commands:
        - subJobType: hpc
          parameters: parameters
        - subJobType: hpc
          parameters: parameters
        status: new
      xml:
        name: Job
    inline_response_200:
      type: object
      allOf:
      - $ref: '#/components/schemas/PaginationResponse'
    body:
      type: object
      properties:
        fileName:
          type: string
          format: binary
  requestBodies:
    Job:
      description: Job object that needs to be added
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Job'
        application/xml:
          schema:
            $ref: '#/components/schemas/Job'
      required: true
  securitySchemes:
    secret_auth:
      type: oauth2
      flows:
        implicit:
          authorizationUrl: ''
          scopes:
            job/find_jobs_by_status_scope: used in /job/findJobsByStatus endpoint
            job/add_job_scope: used in /job endpoint
            job/update_job_by_operation_scope: used in /job/updateByOperation/{jobId} endpoint
            job/get_job_by_id_scope: used in 'GET' /job/{jobId} endpoint
            job/update_job_scope: used in 'PUT' /job/{jobId} endpoint
            job/delete_job_scope: used in 'DELETE' /job/{jobId} endpoint
            user/login_user_scope: used in /user/login endpoint
            file/upload_file_scope: used in /file/uploadFile endpoint
            file/get_file_scope : used in /file/{jobId}/getFile endpoint
      x-tokenInfoFunc: mytok.token_info


