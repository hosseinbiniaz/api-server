#first action in hpc.py is 
#r = requests.post(post_file_url, files=files)
#post_file_url = BASE_URL + '/file/uploadFile?filename='+hpc_job['jobMetaData']['file']+'&accessToken=' + hpc_job['accessToken']

import requests
import yaml
import json
import time
import sys
import os

#HOST = localhost
#PORT = 8080

#terminal:
#base_url="http://localhost:8080/HPC-RESTful/1.0.0"
#curl -X get $base_url"/ui/"
#curl -X post $base_url"/file/uploadFile"

def main():
    BASE_URL="http://localhost:8080/HPC-RESTful/1.0.0"

    #BASE_URL = 'http://'+HOST+':'+PORT+
    yaml_file = 'hpc.yml' #needs to be in same directory

    with open(yaml_file) as file:
        hpc_job = yaml.load(file, Loader=yaml.FullLoader)
        post_file_url = BASE_URL + '/file/uploadFile?filename='+hpc_job['jobMetaData']['file']+'&accessToken=' + hpc_job['accessToken']
        
        old_hpcpy_system_3_arg = 'code.zip'
        print ('Uploading file.')
        # Posting request
        files = {'fileName': open( str(old_hpcpy_system_3_arg) ,'rb')}
        r = requests.post(post_file_url, files=files)
        result = r.json()
        print(result)
        print ('Uploading file completed.')

if __name__ == '__main__':
    main()